var retailers = require('./retailers.json');

module.exports = {

  getRetailer: function(id) {
    for (var retailerKey in retailers.retailers) {
      if (retailers.retailers[retailerKey].id == id) {
        return retailers.retailers[retailerKey]
      }
    }
    return null;
  }

}
