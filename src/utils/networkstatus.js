import {NetInfo} from 'react-native';

var IsOnline = false;

function startListening(onComplete: ?() => boolean) {
  NetInfo.isConnected.addEventListener(
      'change',
      _handleConnectivityChange
  );

  NetInfo.isConnected.fetch().done(
      (isConnected) => {
        IsOnline = isConnected;
        onComplete && onComplete(IsOnline);
      }
  );
};

function stopListening() {
  NetInfo.isConnected.removeEventListener(
      'change',
      _handleConnectivityChange
  );
};

function _handleConnectivityChange (isConnected) {
  IsOnline = isConnected;
};

function isOnline () {
  return IsOnline;
};

exports.startListening = startListening;
exports.stopListening = stopListening;
exports.isOnline = isOnline;
