import {
  AsyncStorage
} from 'react-native';

module.exports = {

  async getSupplierOrder() {
    return await this.getObject('@MySuperStore:supplierorder');
  },

  async storeSupplierOrder(order) {
    return await this.storeObject('@MySuperStore:supplierorder', order);
  },

  async removeSupplierOrder() {
    return await this.removeString('@MySuperStore:supplierorder');
  },

  async getObject(key) {
    const str = await this.getString(key);
    if(str != null) {
      return JSON.parse(str);
    } else {
      return null;
    }
  },

  async storeObject(key, value) {
    return await this.storeString(key, JSON.stringify(value));
  },


  async getString(key) {
    try {
      //console.log("GETTING", key)
      return await AsyncStorage.getItem(key);
    } catch (error) {
      console.warn('Error while reading:');
      console.warn(error);
      return null;
    }
  },

  async storeString(key, value) {

    try {
      //console.log("STORING", key, value);
      await AsyncStorage.setItem(key, value);
    } catch (error) {
      console.warn('Error while saving:');
      console.warn(error);
    }
  },

  async removeString(key) {
    try {
      return await AsyncStorage.removeItem(key);
    } catch (error) {
      console.warn('Error while removing:');
      console.warn(error);
      return null;
    }
  },

}
