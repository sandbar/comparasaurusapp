// var rootUrl = 'http://35.177.226.179/app.php/';
var rootUrl = 'http://www.comparasaurusapp.com/app.php/';

var getJsonHeaders = function() {
  return {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }
};

module.exports = {

  search: function(query, retailer, page) {
    var url = `${rootUrl}search?query=${query}&retailer=${retailer}&page=${page}`;
    // console.log(url)
    return fetch(url)
      .then((response) => response.json());
  },

  deviceID: function(uid) {
    var url = `${rootUrl}add_user?uid=${uid}`;
    // console.log(url)
    return fetch(url)
      .then((response) => response.json());
  },

}
