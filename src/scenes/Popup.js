import React, { Component } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Animated,
  Image
} from 'react-native';

var styles = require('../common/styles');
var {width, height} = require('Dimensions').get('window');
const PADDING = width <= 375 ? 10 : 20;

module.exports = React.createClass({

  getInitialState() {
    return {
      popupOpen: false,
      popupCoverAnim: new Animated.Value(0),
      popupCoverHeight: 0,
      popupAnim: new Animated.Value(height + 16),
    }
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.visible) {
      this.togglePopup();
    } else if (nextProps.data) {
      //
    }
  },

  render() {
    return (
      <View
        style={{flex:1, position: 'absolute', top:0, left:0}}>
        <Animated.View
          style={[styles.popupCover,
            {
              opacity: this.state.popupCoverAnim,
              height: this.state.popupCoverHeight
            }
          ]}>
          <TouchableOpacity
            onPress={() => this.togglePopup()}
            style={{flex:1}}/>
        </Animated.View>

        <Animated.View style={[styles.popupContainer, {top: this.state.popupAnim}]}>
          <TouchableOpacity onPress={this.togglePopup} style={styles.popupClose}>
            <Image source={require('../../img/icon-close.png')} />
          </TouchableOpacity>
          <Image source={require('../../img/dino.png')} style={styles.popupDino} />
          <Text style={styles.popupText}>Go to {this.props.retailerChosen} to view this item?</Text>
          <View style={styles.popupButtonContainer}>
            <TouchableOpacity onPress={this.getURL} style={styles.buttonDefault}>
              <Text style={styles.buttonDefaultText}>YES</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.togglePopup} style={styles.buttonDefault}>
              <Text style={styles.buttonDefaultText}>NO</Text>
            </TouchableOpacity>
          </View>
        </Animated.View>

      </View>
    )
  },

  togglePopup() {

    if(this.state.popupOpen) {
      this.props.resetPopupVisibility();
    }

    let initialCoverValue = this.state.popupOpen ? 1 : 0,
        finalCoverValue = this.state.popupOpen ? 0 : 1;
    let initialPopupValue = this.state.popupOpen ? (height * 0.5) - 102 : height + 16,
        finalPopupValue = this.state.popupOpen ? height + 16 : (height * 0.5) - 102;

    this.state.popupCoverAnim.setValue(initialCoverValue);
    Animated.spring(
      this.state.popupCoverAnim,
      {
        toValue: finalCoverValue
      }
    ).start();

    this.state.popupAnim.setValue(initialPopupValue);
    Animated.spring(
      this.state.popupAnim,
      {
        toValue: finalPopupValue
      }
    ).start();

    this.setState({
      popupCoverHeight: this.state.popupOpen ? 0 : height,
      popupOpen: !this.state.popupOpen,
    });
  },

  getURL() {
    this.props.showModal(true)
  },


});
