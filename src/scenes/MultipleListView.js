import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight,
  Animated,
  Image,
  TextInput,
  ListView
} from 'react-native';

import {Actions} from 'react-native-router-flux';

var styles = require('../common/styles');

let data1 = {
  hello: { text: 'world' },
  how: { text: 'are you' },
  test: { text: 123 },
  this: { text: 'is' },
  a: { text: 'a' },
  real: { text: 'real' },
  drag: { text: 'drag and drop' },
  bb: { text: 'bb' },
  cc: { text: 'cc' },
  dd: { text: 'dd' },
  ee: { text: 'ee' },
  ff: { text: 'ff' },
  gg: { text: 'gg' },
  hh: { text: 'hh' },
  ii: { text: 'ii' },
  jj: { text: 'jj' },
  kk: { text: 'kk' },
};

let data2 = {
  hello: { text: 'world' },
  how: { text: 'are you' },
  test: { text: 123 },
  this: { text: 'is' },
  a: { text: 'a' },
  real: { text: 'real' },
  drag: { text: 'drag and drop' },
  bb: { text: 'bb' },
  cc: { text: 'cc' },
  dd: { text: 'dd' },
  ee: { text: 'ee' },
  ff: { text: 'ff' },
  gg: { text: 'gg' },
  hh: { text: 'hh' },
  ii: { text: 'ii' },
  jj: { text: 'jj' },
  kk: { text: 'kk' },
};

module.exports = React.createClass({

  getInitialState() {
    ds1 = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    ds2 = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return {
      loaded: false,
      dataSource1: ds1.cloneWithRows(data1),
      dataSource2: ds2.cloneWithRows(data2)
    }
  },

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <TextInput />
        </View>
        <View style={styles.contentContainer}>
          <ListView
            dataSource={this.state.dataSource1}
            style = {styles.list}
            renderRow={this.renderRow}
            horizontal
            />
          <ListView
            dataSource={this.state.dataSource2}
            style = {styles.list}
            renderRow={this.renderRow}
            horizontal
            />
        </View>
      </View>
    )
  },

  renderRow(rowData, sectionID, rowID) {
    return (
      <TouchableHighlight onPress={() => this.pressRow(rowData)} underlayColor={'#FFF'}>
        <Text>{rowData.text}</Text>
      </TouchableHighlight>
    );
  },

  pressRow(data) {
    console.log(data);
  }

});
