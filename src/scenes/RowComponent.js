import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight,
  Animated,
  Image,
  TextInput,
  ListView,
  Easing
} from 'react-native';

import {Actions} from 'react-native-router-flux';

var API = require('../communication/api');
var styles = require('../common/styles')
var CONSTANTS = require('../data/Constants');
const {width, height} = require('Dimensions').get('window');
const BLANK_DATA_PHONE = [{
  "product_id": 999999999999999,
  "product_name": "",
  "product_url": "https://",
  "image_url": "https://",
  "price": ""
}];
const BLANK_DATA_TABLET = [{
  "product_id": 999999999999999,
  "product_name": "",
  "product_url": "https://",
  "image_url": "https://",
  "price": ""
},
{
  "product_id": 999999999999999,
  "product_name": "",
  "product_url": "https://",
  "image_url": "https://",
  "price": ""
}];

module.exports = React.createClass({

  getInitialState() {
    return {
      loaded: false,
      dataSource: null,
      retailerData: CONSTANTS.getRetailer(this.props.data.id),
      page: 1,
      spinValue: new Animated.Value(0),
      renderSpace: false
    }
  },

  componentWillMount() {
    this.fetchData();
  },

  fetchData() {
    API.search( this.props.query, this.props.data.id, this.state.page )
      .then((searchResultsJson) => {
        // SUCCESS
        if (searchResultsJson.length > 0) {
          this.prepareData(searchResultsJson);
        } else if (this.state.page > 1 && !this.state.renderSpace) {
          this.prepareData(width <= 375 ? BLANK_DATA_PHONE : BLANK_DATA_TABLET);
          //console.log("RENDER SPACE")
          this.setState({
            renderSpace: true
          })
        } else {
          this.setState({
            loaded: true
          })
        }

      })
      .catch((error) => {
        console.warn(error);
      });
  },

  prepareData(data) {
    ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    var dataBlob = this.state.productsData == null ? [] : this.state.productsData;
    for (var i = 0; i < data.length; i++) {
      dataBlob.push(data[i]);
    }
    this.setState({
      dataSource: ds.cloneWithRows(dataBlob),
      productsData: dataBlob,
      loaded: true
    });
  },

  render() {
      var gap = this.props.order[this.props.order.length-1] == this.state.retailerData.icon ? 16 : 8;
      return (
        <View style={[styles.shopContainer, {marginBottom : gap}]}>
          <TouchableHighlight
            underlayColor={'#eee'}
            style={[styles.shopBrand, {backgroundColor: this.state.retailerData.bgColor }]}
            {...this.props.sortHandlers}
            delayLongPress={10}
            >
            <Image source={this.renderIcon(this.state.retailerData.icon)} />
          </TouchableHighlight>

          {this.renderView()}

        </View>
      )
  },

  renderView() {
    if (this.state.dataSource !== null) {
      if (this.state.loaded) {
        return this.renderListView();
      } else {
        return this.renderLoadingView();
      }
    } else {
      return (
        <View style={styles.shopLoadingContainer}>
          <View style={styles.warningTextBG}>
            <Text style={{fontSize:12}}>No products!</Text>
          </View>
        </View>
      )
    }
  },

  renderListView() {
    return (
      <ListView
        dataSource={this.state.dataSource}
        style = {styles.shopList}
        renderRow={this.renderRow}
        horizontal
        onEndReached={this.onEndReached}
        onEndReachedThreshold={700}
        />
    )
  },

  renderRow(rowData, sectionID, rowID) {
    imgSrc = rowData.image_url;
    return (
      <TouchableHighlight
        onPress={() => this.pressRow(rowData)}
        underlayColor={'#FFF'}
        >
        <View style={styles.productContainer}>
          <Image source={{uri:imgSrc}} style={styles.productImage}/>
          <Text
            numberOfLines={4}
            style={styles.productInfo}
            >
            {rowData.product_name}
          </Text>
          <Text style={styles.productPrice}>{rowData.price}</Text>
        </View>
      </TouchableHighlight>
    );
  },

  renderIcon(type) {
    switch (type) {
      case 'tesco' :
        return require('../../img/icon-tesco.png');
        break;
      case 'asda' :
        return require('../../img/icon-asda.png');
        break;
      case 'ocado' :
        return require('../../img/icon-ocado.png');
        break;
      case 'waitrose' :
        return require('../../img/icon-waitrose.png');
        break;
      case 'sainsburys' :
        return require('../../img/icon-sainsburys.png');
        break;
      case 'morrisons' :
        return require('../../img/icon-morrisons.png');
        break;
      case 'iceland' :
        return require('../../img/icon-iceland.png');
        break;
      case 'aldi' :
        return require('../../img/icon-aldi.png');
        break;
      case 'lidl' :
        return require('../../img/icon-lidl.png');
        break;
    }
  },

  renderLoadingView() {
    return (
      <View style={styles.shopLoadingContainer}>
        <View style={styles.warningTextBG}>
          <Text style={{fontSize:12}}>Loading...</Text>
        </View>
      </View>
    )
  },

  onEndReached() {
    newPage = parseInt(this.state.page + 1);
    console.log("the end!! get page " + newPage)
    this.setState({
      page: newPage
    })
    setTimeout(this.fetchData, 1000);
  },

  pressRow(data) {
    this.props.handleProductPress(data, this.state.retailerData.name)
  }

});
