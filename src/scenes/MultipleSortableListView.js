import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight,
  Animated,
  Image,
  TextInput,
  ListView
} from 'react-native';

import SortableListView from 'react-native-sortable-listview';
import {Actions} from 'react-native-router-flux';

var styles = require('../common/styles');
var RowComponent = require('./RowComponent');

let data = {
  data1: {
    j: { text: 'world1' },
    how: { text: 'are you' },
    test: { text: 123 },
    this: { text: 'is' },
    a: { text: 'a' },
    real: { text: 'real' },
    drag: { text: 'drag and drop' },
    bb: { text: 'bb' },
    cc: { text: 'cc' },
    dd: { text: 'dd' },
    ee: { text: 'ee' },
    ff: { text: 'ff' },
    gg: { text: 'gg' },
    hh: { text: 'hh' },
    ii: { text: 'ii' },
    jj: { text: 'jj' },
    kk: { text: 'kk' }
  },
  data2: {
    j: { text: 'world2' },
    how: { text: 'are you' },
    test: { text: 123 },
    this: { text: 'is' },
    a: { text: 'a' },
    real: { text: 'real' },
    drag: { text: 'drag and drop' },
    bb: { text: 'bb' },
    cc: { text: 'cc' },
    dd: { text: 'dd' },
    ee: { text: 'ee' },
    ff: { text: 'ff' },
    gg: { text: 'gg' },
    hh: { text: 'hh' },
    ii: { text: 'ii' },
    jj: { text: 'jj' },
    kk: { text: 'kk' }
  }
}

let order = Object.keys(data);

module.exports = React.createClass({

  getInitialState() {
    return {
      loaded: false
    }
  },

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <TextInput />
        </View>
        <View style={styles.contentContainer}>
          <SortableListView
              style={{ flex: 1 }}
              data={data}
              order={order}
              onRowMoved={e => {
                order.splice(e.to, 0, order.splice(e.from, 1)[0])
                this.forceUpdate()
              }}
              renderRow={row => <RowComponent data={row} />}
            />
        </View>
      </View>
    )
  }

});
