import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight,
  Animated,
  Image,
  TextInput,
  ListView,
  Modal,
  WebView
} from 'react-native';

import {Actions} from 'react-native-router-flux';
import SortableListView from 'react-native-sortable-listview';
import DeviceInfo from 'react-native-device-info';

var API = require('../communication/api');
var styles = require('../common/styles');
var Popup = require('./Popup');
var RowComponent = require('./RowComponent');
var AsyncStorage = require('../communication/AsyncStorage');

let data = {
  tesco: {
    id: 1
  },
  sainsburys: {
    id: 7
  },
  asda: {
    id: 3
  },
  morrisons: {
    id: 5
  },
  aldi: {
    id: 2
  },
  waitrose: {
    id: 8
  },
  lidl: {
    id: 9
  },
  iceland: {
    id: 4
  },
  ocado: {
    id: 6
  }
}

let dataOrder = Object.keys(data);

module.exports = React.createClass({

  getInitialState() {
    return {
      initialLoad: true,
      spinValue: new Animated.Value(0),
      popupVisible: false,
      popupData: {},
      data: data,
      order: dataOrder,
      searchString: '',
      modalVisible: false,
      isHelpModal: false
    }
  },

  async componentDidMount() {
    var order = await AsyncStorage.getSupplierOrder();
    if (order) {
      // console.log('getting stored...', order);
      this.setState({order:order})
    } else {
      this.setState({order:dataOrder})
    }
    // send UDID
    this.sendDeviceID();
  },

  componentDidUpdate(props, state) {
    if (state.data) {
      AsyncStorage.storeSupplierOrder(state.order);
      // console.log('storing...', state.order)
    }
  },

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <Image source={require('../../img/dino.png')} style={styles.dino}/>
          <View style={styles.brandContainer}>
            <View style={styles.logoContainer}>
              <Image source={require('../../img/brand.png')} />
            </View>
            <TouchableOpacity style={styles.helpButton} onPress={this.handleHelpPress}>
              <Text style={styles.helpButtonText}>?</Text>
            </TouchableOpacity>
          </View>
          <TextInput
            style={styles.textInput}
            autoFocus
            returnKeyType={'search'}
            placeholder={'Enter a search term'}
            onChangeText={(text) => this.setState({searchString: text, initialLoad:true})}
            onSubmitEditing={() => setTimeout(this.handleSubmit, 500)}
            ref='searchField'
            clearButtonMode={'always'}
            underlineColorAndroid='transparent'
            />
          <Image source={require('../../img/icon-search.png')} style={styles.searchIcon} />
        </View>
        {this.getRenderView()}
        <Popup
          visible={this.state.popupVisible}
          resetPopupVisibility={this.resetPopupVisibility}
          data={this.state.popupData}
          retailerChosen={this.state.retailerChosen}
          showModal={this.setModalVisible}/>
       <Modal
         animationType={"slide"}
         transparent={false}
         visible={this.state.modalVisible}
         onRequestClose={() => this.onRequestClose}>
          <View style={{flex:1}}>
            <View style={styles.modalHeader}>
              <Image source={require('../../img/dino.png')} style={[styles.dino, {opacity: this.state.isHelpModal ? 0 : 1}]}/>
              <TouchableHighlight
                onPress={() => { this.setModalVisible(!this.state.modalVisible)}}
                style={styles.popupClose}>
               <Image source={require('../../img/icon-close.png')} />
              </TouchableHighlight>
            </View>
            <WebView
              source={{uri: this.state.retailerURL}}
              style={styles.webView}/>
           </View>
         </Modal>
      </View>
  )},

  getRenderView() {
    if (this.state.initialLoad) {
      return this.renderInitialLoadView();
    } else {
      return this.renderListView();
    }
  },

  renderInitialLoadView() {
    return (
      <View style={styles.bubbleContainer}>
        <Image source={require('../../img/dinobubble.png')} >
        <Text style={styles.bubbleText}>    Hi, what are you looking for?</Text>
        </Image>
      </View>
    )
  },

  renderListView() {
    return (
      <View style={styles.contentContainer}>
        <SortableListView
            style={styles.listView}
            data={this.state.data}
            order={this.state.order}
            onRowMoved={e => {
              this.state.order.splice(e.to, 0, this.state.order.splice(e.from, 1)[0])
              this.forceUpdate()
            }}
            sortRowStyle={styles.sortRowStyle}
            renderRow={row => <RowComponent data={row} query={this.state.searchString} handleProductPress={this.handleProductPress} order={this.state.order} />}
          />
      </View>
    )},

    handleSubmit() {
      if (this.state.searchString !== '') {
        this.setState({
          initialLoad:false
        });
        this.refs.searchField.blur()
      }
    },

    handleHelpPress() {
      //this.handleSubmit();
      console.log('help pressed');
      this.setState({
        retailerURL: 'http://www.comparasaurus.com/help.htm',
        modalVisible: true,
        isHelpModal: true
      })
    },

    handleProductPress(data, retailer) {
      console.log(data.product_url)
      this.setState({
        popupVisible: true,
        popupData: data,
        retailerChosen: retailer,
        retailerURL: data.product_url,
        isHelpModal: false
      });
    },

    sendDeviceID() {
      var uid = DeviceInfo.getUniqueID();
      console.log(uid);
      API.deviceID( uid )
        .then((userJson) => {
          // SUCCESS
          console.log(userJson);
          /*if (userJson.length > 0) {
            this.prepareData(userJson);
          } */
        })
        .catch((error) => {
          console.warn(error);
        });
    },

    onRequestClose() {

    },

    setModalVisible(visible) {
      this.setState({modalVisible: visible});
    },

    resetPopupVisibility() {
      this.setState({popupVisible:false});
    },

});
