var React = require('React');
var NetworkStatus = require('./utils/networkstatus');
import Main from './Main';

function setup(): ReactClass<{}> {
  // console.disableYellowBox = true;

  class Root extends React.Component {
    state: {
      isReady: boolean;
    };

    constructor() {
      super();
      this.state = {
        isReady: false
      };
    }

    /*async componentDidMount() {
      // Looking for token
      var token = await AsyncStorage.getToken();
      if(token) {
        //console.log("token", token)
        UserProxy.storeToken(token);
      }

      // Listen for Network Status changes
      NetworkStatus.startListening( (status) => this.setIsReady() );
    }*/

    componentDidMount() {
      NetworkStatus.startListening( (status) => this.setIsReady() );
    }

    setIsReady() {
      // Small delay so we're sure status is up to date
      setTimeout(
        () => this.setState({isReady: true}),
        100);
    }

    render() {
      if (this.state.isReady) {
        return (
          <Main />
        );
      }
      return null;
    }
  }

  return Root;
}

module.exports = setup;
