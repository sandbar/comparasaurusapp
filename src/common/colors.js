'use strict';

let Colors = {
  green: '#9afcc1',
  greenMid: '#579b6e',
  greenDark: '#103d1a'
};

export default Colors;
