import React, { Component } from 'react';
import {
  Navigator,
  PixelRatio,
  StyleSheet,
  Platform
} from 'react-native';

import Colors from './Colors';

// get dimensions
const {width, height} = require('Dimensions').get('window');

const PADDING = width <= 375 ? 8 : 16;

module.exports = StyleSheet.create({

  /*** GENERIC ***/

  mainContainer:{
    flex:1,
    backgroundColor: Colors.green
  },
  paddedContainer: {
    flex:1,
    paddingLeft: PADDING,
    paddingRight: PADDING,
    backgroundColor: Colors.green
  },
  loadingContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.green,
    marginBottom: 110
  },
  textInput: {
    backgroundColor: '#FFF',
    borderRadius: 6,
    padding: 10,
    paddingLeft: 36,
    height: 40
    //marginTop: (Platform.OS === 'ios') ? 0 : 16
  },
  popupCover: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    width: width,
    height: height,
    position: 'absolute',
    top: 0,
    overflow: 'hidden',
    flex: 1
  },
  popupContainer: {
    backgroundColor: Colors.green,
    width: width - (4 * PADDING),
    height: 204,
    position: 'absolute',
    left: 2 * PADDING,
    top: height + 16,
    borderRadius: 6,
    padding: 2 * PADDING,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  popupText: {
    paddingTop: 24,
    paddingBottom: 12,
    fontSize: 18,
  },
  popupButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  buttonDefault: {
    borderRadius: 6,
    backgroundColor: '#FFF',
    margin: 10,
    height: 42,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonDefaultText: {
    color: '#000'
  },
  popupClose: {
    position: 'absolute',
    right: 16,
    top: 16,
    width: 26,
    height: 26,
    borderRadius: 13,
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'center'
  },
  popupCloseText: {
    color: '#000',
    fontSize: 18,
  },
  popupDino: {
    position: 'absolute',
    top: (Platform.OS === 'ios') ? -16 : 6,
    left: (Platform.OS === 'ios') ? (width * 0.5) - 55 : (width * 0.5) - 50,
    width: (Platform.OS === 'ios') ? 50 : 39,
    height: (Platform.OS === 'ios') ? 78 : 60
  },

  /*** CONTENT ***/

  headerContainer: {
    height: 102,
    width: width,
    backgroundColor: Colors.green,
    paddingLeft: PADDING,
    paddingRight: PADDING,
    borderBottomWidth: 8,
    borderBottomColor: Colors.green
  },
  contentContainer: {
    flex:1,
    backgroundColor: Colors.green
  },
  bubbleContainer: {
    flex:1,
    paddingLeft: PADDING,
    paddingRight: PADDING,
    alignItems: 'center',
    marginTop: 50
  },
  brandContainer: {
    flex: 1,
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 52,
  },
  logoContainer: {
    flex:1,
    alignItems: 'center',
    marginLeft: 44
  },
  dino: {
    position: 'absolute',
    top: 10,
    left: PADDING + 8
  },
  bubbleText: {
    position: 'absolute',
    backgroundColor: 'transparent',
    top: 30,
    fontSize: 18
  },
  helpButton: {
    width: 22,
    height: 22,
    borderRadius: 11,
    marginRight: 3,
    backgroundColor: Colors.greenDark,
    justifyContent: 'center',
    alignItems: 'center',
  },
  helpButtonText: {
    fontSize: 16,
    color: Colors.green,
    fontWeight: 'bold'
  },
  searchIcon: {
    position: 'absolute',
    top: 65,
    left: PADDING + 8
  },
  listView: {
    padding: PADDING
  },
  sortRowStyle: {
    opacity: 0.6
  },
  shopContainer: {
    flexDirection:'row',
    backgroundColor: '#FFF',
    borderRadius: 6,
    marginBottom: 8,
    elevation: 1,
    shadowColor: "#000000",
    shadowOpacity: 0.3,
    shadowRadius: 1,
    height: 110,
    shadowOffset: {
      height: 1,
      width: 0
    }
  },
  shopBrand: {
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6
  },
  shopList: {
    //height: 110
  },
  shopLoadingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex:1
  },
  productContainer: {
    flexDirection: 'row',
    padding: 8,
    width: 260
  },
  productImage: {
    width: 80,
    height: 80,
    margin: 5,
    resizeMode: 'contain'
  },
  productInfo: {
    fontSize: 14,
    width: 160,
    marginTop: 8,
    lineHeight: 16
  },
  productPrice: {
    fontSize: 14,
    fontWeight: 'bold',
    position: 'absolute',
    bottom: 8,
    left: 100,
  },
  modalHeader: {
    width: width,
    height: 54,
    backgroundColor: Colors.green,
    overflow: 'hidden'
  },
  webView: {
    flex:1,
    width:width,
    height:height,
    backgroundColor: Colors.green
  },
  warningTextBG: {
    backgroundColor: '#e1e1e1',
    borderRadius: 20,
    padding: 4,
    paddingLeft: 10,
    paddingRight: 10
  }
});
