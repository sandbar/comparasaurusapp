var React = require('react');
var ReactNative = require('react-native');

import {
  StatusBar
} from 'react-native';

import {
  Actions,
  Scene,
  Router,
  ActionConst
} from 'react-native-router-flux';

// var styles = require('./common/styles');

// import Search from './scenes/SortableListView';
// import Search from './scenes/MultipleListView';
// import Search from './scenes/MultipleSortableListView';
import Search from './scenes/Search';

var ROUTES = {
  search: Search
};

var Main = React.createClass({

  componentWillMount: function() {
    StatusBar.setHidden(true);
  },

  render: function() {
    //Orientation.lockToPortrait();
    return (
      <Router>
        <Scene key="root" hideNavBar={true}>
          <Scene key="main" panHandlers={null}>

          <Scene
            key="search"
            component={Search}
            panHandlers={null}
            hideNavBar
            initial
            //direction="vertical"
          />

          </Scene>
        </Scene>
      </Router>
    )
  }
});

module.exports = Main;
